

void send_USART(uint8_t data)
{
	/* Wait for empty transmit buffer */
	while ( !(UCSR0A & (1<<UDRE0)) )
	;
	
	/* Put data into buffer (sends the data) */
	UDR0 = data;
}

void send_string_UART(char* string) {
	int i = 0;
	while(string[i] != '\0') {
		send_USART(string[i]);
		i++;
	}
	
	send_USART(10); //newline
	send_USART(13); //enter
}

void init_USART(void)
{
	/* Set baud rate */
	DDRD |= (1<<PIND0);
	DDRD &= ~(1<<PIND1);
	UBRR0 = 51;// this is from the formula FCPU/16/BAUD-1 which is ~~51
	
	/* Enable receiver and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	
	/* Set frame format: 8 data bits */
	UCSR0C = (1<<UCSZ01) | (1<<UCSZ00);
}

char read_USART(void)
{
	/* Wait for data to be received */
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	
	/* Get and return received data from buffer */
	//return UDR0;
	//char Result = UDR0;
	return UDR0;
}