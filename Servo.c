void init_servo_1(void)

{
	/* Servo pin is PB1 */
	DDRB |= (1 << PORTB1);
	
	/* Use Fast PWM mode, comparing to ICR1 */
	TCCR1A |= (1<<WGM11);
	TCCR1B |= (1<<WGM12)|(1<<WGM13);
	ICR1 = 2500;
	
	/* Enable channel A, set to inverting mode */
	TCCR1A |= (1<<COM1A1);
	
	/* Set prescaler (8MHz / 1 = 8MHz) */
	TCCR1B |= (1<<CS11)|(1<<CS10);
	
	/* Initial value */
	OCR1A = 307;
	//  Between 70 and 500 so move in values of 1
}

void init_servo_2(void)
{	
	/* Servo pin is PB3 */
	DDRB |= (1 << PORTB3);
	/*Use Fast PWM mode, comparing to 0xFF*/
	TCCR2A = (1<<COM2A1) | (1<<COM2B1) | (1<<WGM21) | (1<<WGM20);
	TCCR2B = (1<<CS22)|(1<<CS21);
	OCR2A = 60;
}

void move_servo(int servo, char dir) {
	/* servo = 1 or 2 dir = u or d */
	/* Servo 1/Arm Servo  */
	if (servo == 1 ) {
		/* Up */
		if (dir == 'u') {
			OCR1A = OCR1A-1;
		} 
		/* Down */
		else if (dir == 'd'&&OCR1A<331) {
			OCR1A = OCR1A+1;
		}
	}	
	/* Servo 2/Scoop Servo  */
	else if (servo == 2 ) {
		/* Up */
		if (dir == 'u'&& OCR2A<64) {
			OCR2A = OCR2A+1;
		}
		/* Down */
		else if (dir == 'd') {
			OCR2A = OCR2A-1;
		}
	}
	
}
void preset(int Preset)
{
	if (Preset == 1)
	{	if(OCR1A<315) // If the arm is above defauls
		while (OCR1A<315)
		{OCR1A = OCR1A+2; // Lower the arm until it reaches the position
			if (OCR2A <60)// if the scoop is below default
			{OCR2A = OCR2A+1; // increase scoop
			}
			if (OCR2A>60)// if the scoop is above default
			{OCR2A = OCR2A-1; // decreasse scoop
			}
			_delay_ms(15);
		}
		if(OCR1A>315)
		while (OCR1A<315)
		{OCR1A = OCR1A-2;
			if (OCR2A <60)
			{OCR2A = OCR2A+1;
			}
			if (OCR2A>60)
			{OCR2A = OCR2A-1;
			}
						_delay_ms(15);

		}
		 

	}
}