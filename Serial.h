/*
serial.h - Communication via serial port
*/

#include <avr/io.h>

#define FOSC 8000000                // clock speed
#define BAUD 9600                   // baud rate
#define MYUBRR (FOSC/16/BAUD-1)     // asynchronous normal mode

void init_USART(void);
void send_USART(uint8_t);
char read_USART(void);
void flush_USART(void);
void send_string_UART(char*);
void convert_to_ascii(int);
