*
servo.h - Servo control
*/

#include <avr/io.h>
#include <util/delay.h>

void init_servo_1(void);
void init_servo_2(void);
void move_servo_One(uint8_t);
void move_servo_Two(uint8_t);