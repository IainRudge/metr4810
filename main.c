/*
 * Main.c
 * Last Updated 29/05/16
 * Author : Iain Rudge , Justin Ho 
 */ 
#define  F_CPU  8000000UL // Defines the microcontroller's clock rate to be 8Mhz
#include <avr/io.h> 
#include <util/delay.h>
#include "Servo.h"
#include "Serial.h"

#define M1A PORTC1 //M1A is one of the Direction pins for motor 1
#define M1B PORTC4	//M1B is one of the Direction pins for motor 1
#define M2A PORTC2	//M2A is one of the Direction pins for motor 2
#define M2B PORTC3	//M2B is one of the Direction pins for motor 2
#define	LED	PORTC5 //Sets the pin controlling the LED 
int main(void)

{/*
* Initialise all functions and pins 
*/
	init_USART(); 
	init_servo_1();
	init_servo_2();
	
	DDRB |= (1<<PORTB2)|(1<<PORTB1)|(1<<PORTB0);	// Set B-ports 2,1,0 as Outputs
	DDRC |= (1<<PORTC1)|(1<<PORTC2)|(1<<PORTC3)|(1<<PORTC4)|(1<<PORTC5); // Set C-ports 1-5 as Outputs
	
    while(1) 
	{	PORTC = (1<<LED); // LED is always on 
		char Res = read_USART(); // Res is set as the char read from USART
		//If Command is k then move Servo 1 down
		if (Res == 'k')
		{
			move_servo(1, 'd');
		}
		//If Command is p then move Servos to preset position
		else if (Res == 'p')
		{
			preset(1);
		}
		//If Command is 1 then move Servo 1 up
		else if (Res =='i' )
		{move_servo(1, 'u');
		}
		//If Command is u then move Servo 2 up
		else if (Res =='u' )
		{move_servo(2, 'u');
		}
		//If Command is j then move Servo 2 down
		else if (Res =='j' )
		{move_servo(2, 'd');
		}
	
		 else if (Res == 'w')
			 // Set pins to drive forward for 30ms
		 {	 PORTC &= ~(1<<M1A);
			 PORTC |= (1<<M1B);
			 PORTC &= ~(1<<M2A);
			 PORTC |= (1<<M2B);
			 _delay_ms(30);		
			 // Turn the motors off, allowing slight movement on tap of keys
			  PORTC &= ~(1<<M1A);
			  PORTC &= ~(1<<M1B);
			  PORTC &= ~(1<<M2A);
			  PORTC &= ~(1<<M2B); 
		 }
		else if (Res == 's')
		// Set pins to drive backward for 30ms
		{PORTC |= (1<<M1A);
		PORTC &= ~(1<<M1B);
		PORTC |= (1<<M2A);
		PORTC &= ~(1<<M2B);
		_delay_ms(30);
		// Turn the motors off, allowing slight movement on tap of keys
		 PORTC &= ~(1<<M1A);
		 PORTC &= ~(1<<M1B);
		 PORTC &= ~(1<<M2A);
		 PORTC &= ~(1<<M2B);
		
		}
		
		else if (Res == 'd')
		{
		// Set pins to drive Right for 30ms
		PORTC &= ~(1<<M1A);
		PORTC |= (1<<M1B);
		PORTC |= (1<<M2A);
		PORTC &= ~(1<<M2B);
		_delay_ms(30);
		// Turn the motors off, allowing slight movement on tap of keys
		 PORTC &= ~(1<<M1A);
		 PORTC &= ~(1<<M1B);
		 PORTC &= ~(1<<M2A);
		 PORTC &= ~(1<<M2B);
		}
		else if (Res == 'a')
		{
		// Set pins to drive Left for 30ms
		PORTC |= (1<<M1A);
		PORTC &= ~(1<<M1B);
		PORTC &= ~(1<<M2A);
		PORTC |= (1<<M2B);
		 _delay_ms(30);	
		 // Turn the motors off, allowing slight movement on tap of keys
		  PORTC &= ~(1<<M1A);
		  PORTC &= ~(1<<M1B);
		  PORTC &= ~(1<<M2A);
		  PORTC &= ~(1<<M2B);
		
		}
	}
}


